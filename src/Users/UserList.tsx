import { useState } from "react";
import { User } from "../mock-user";

interface UserListProps {
    users: User[];
}

export const UserList = ({ users }: UserListProps) => {
    const [userClickedIds, setUserClickedIds] = useState<Record<number, boolean>>({});

    const onUserClick = (id: number) => {
        if (userClickedIds[id] === true) {
            setUserClickedIds({ ...userClickedIds, [id]: false });
        }
        else {
            setUserClickedIds({ ...userClickedIds, [id]: true });
        }
    }

    return (
        <div className="user-list-wrapper">
            <div className="user-list">
                <ul>
                    {users.map((user, index) => {
                        return (
                            <li key={index} onClick={() => onUserClick(user.id)} className={user.isHidden ? "user user-hide" : "user"}>
                                <article>
                                    <button className="user__trigger">
                                        <div className="user__image">
                                            <img src={user.img} />
                                        </div>
                                        <div className="user__info">
                                            <h2>{user.firstname} {user.lastname}</h2>
                                            <p className="user__age">{user.age}</p>
                                        </div>
                                    </button>
                                    {userClickedIds[user.id] && <div className="user__age">Extra info: {user.extraInfo}</div>}
                                </article>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div >
    )
}