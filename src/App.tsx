import { useState } from 'react';
import { UserList } from './Users/UserList';
import './App.css';
import { users } from './mock-user';

export const App = () => {
  const [searchText, setSearchText] = useState('');
  const [sortByAge, setSortByAge] = useState(false);

  const onSortByAgeButtonClick = () => setSortByAge(true);

  const nameMatchesSearchText = (name: string) => name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1

  const filteredUsers = users.map((user) => {
    return {
      ...user,
      isHidden: searchText.length >= 3 && !nameMatchesSearchText(user.firstname)
    }
  })

  if (sortByAge) {
    filteredUsers.sort((a, b) => parseInt(b.age) - parseInt(a.age));
  }

  filteredUsers.sort((a, b) => {
    if (a.isHidden) {
      return 1;
    }
    return -1;
  })

  const visibleUsers = filteredUsers.filter((user) => !user.isHidden);

  return (
    <div className="codeflair-assignment">
      <section className="user-filter">
        <label htmlFor="input-field">Filter Users by Name</label>
        <input id="input-field" value={searchText} onChange={(event) => setSearchText(event.target.value)} placeholder={"Search users"}></input>
        {visibleUsers.length === 0 && <span className="user-filter__users-found">results are empty</span>}
        {(searchText.length > 2 && visibleUsers.length > 0) && <span className="user-filter__users-found"> Users found by filtering: {visibleUsers.length}</span>}
        <button className="filter-button" onClick={onSortByAgeButtonClick}>Sort by Age</button>
      </section>
      {visibleUsers.length > 0 && <UserList users={filteredUsers} />}
    </div>
  );
}